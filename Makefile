# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/03/10 21:01:03 by thugueno          #+#    #+#              #
#    Updated: 2023/05/09 21:11:29 by thugueno         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

##########################################
####	MAKE CONF						##

.DEFAULT_GOAL	:=	all

##########################################
####	FILES							##

#GOALS

NAME	:=	pipex

BONUS	:=	pipex_bonus

LIBFT	:=	libft/libft.a

#DIRECTORY

INCLUDE	:=	include/

BUILD	:=	.build/

SRC		:=	src/

MAND_D	:=	mandatory/

BONUS_D	:=	bonus/

#PREREQUISITES

SRCS		:=	src/mandatory/pipex.c				\
				src/mandatory/files_compute.c		\
				src/mandatory/error_handler.c		\
				src/mandatory/exit_program.c		\
				src/mandatory/init.c				\
				src/mandatory/init_utils.c			\
				src/mandatory/execute_cmds.c		\
				src/mandatory/execute_last_cmd.c	\
				src/mandatory/execute_first_cmd.c	\

SRCS_BONUS	:=	src/bonus/pipex_bonus.c				\
				src/bonus/files_compute_bonus.c		\
				src/bonus/error_handler_bonus.c		\
				src/bonus/exit_program_bonus.c		\
				src/bonus/init_bonus.c				\
				src/bonus/init_utils_bonus.c		\
				src/bonus/execute_cmds_bonus.c		\
				src/bonus/execute_last_cmd_bonus.c	\
				src/bonus/execute_first_cmd_bonus.c	\
#TARGETS

OBJS		:=	${SRCS:%.c=${BUILD}%.o}

OBJS_BONUS	:=	${SRCS_BONUS:%.c=${BUILD}%.o}

DEPS		:=	${SRCS:%.c=${BUILD}%.d}

DEPS_BONUS	:=	${SRCS_BONUS:%.c=${BUILD}%.d}

##########################################
####	FLAGS							##

CC			:=	clang

CFLAGS		:=	-Werror -Wextra -Wall

ifdef DEBUG
	CFLAGS +=  -g
else
	CFLAGS +=  -O3
endif

CPPFLAGS	:= -MP -MMD -I${INCLUDE} -I${dir ${LIBFT}}include/

LDFLAGS		:=	-L${dir ${LIBFT}} -l:${notdir ${LIBFT}}

##########################################
####	DECORATION						##

#TEXT

INDENT		:=	[${NAME}]:

#COLORS

CCOMPILE	:=	\033[36m

CCREATE		:=	\033[95m

CDONE		:=	\033[5;32m

CFCLEAN		:=	\033[31m

CCLEAN		:=	\033[33m

CCTAGS		:=	\033[38;5;200m

NOCOLOR		:=	\033[0m

##########################################
####	CUSTOM COMMANDS					##

RM			:=	rm -f

MAKE		:=	@make --no-print-directory TAB="${INDENT}"

ifdef DEBUG
	MAKE := ${MAKE} DEBUG=1 -C
else
	MAKE := ${MAKE} -C
endif

##########################################
####	MANDATORY TARGET				##

all:		libraries ${NAME}

${NAME}:	${OBJS}
			@echo "${CCREATE}${INDENT} Creating ${NAME}${NOCOLOR}"
			@${CC} ${CFLAGS} ${OBJS} ${LDFLAGS} -o ${NAME}
			@echo "${CDONE}${INDENT} ${NAME} done.${NOCOLOR}"

clean:		ctags
			${MAKE} ${dir ${LIBFT}} clean
			@${RM} -r ${BUILD}
			@echo "${CCLEAN}${INDENT} Removing objects${NOCOLOR}"

fclean:		clean
			@${MAKE} ${dir ${LIBFT}} fclean
			@${RM} ${NAME}
			@${RM} ${BONUS}
			@echo "${CFCLEAN}${INDENT} Removing ${NAME}${NOCOLOR}"

re:			fclean all

-include ${DEPS}

.PHONY:		all clean fclean re

##########################################
####	BONUS TARGET					##

bonus:		libraries ${BONUS}

${BONUS}:	${OBJS_BONUS}
			@echo "${CCREATE}${INDENT} Creating ${BONUS}${NOCOLOR}"
			@${CC} ${CFLAGS} ${OBJS_BONUS} ${LDFLAGS} -o ${BONUS}
			@echo "${CDONE}${INDENT} ${BONUS} done.${NOCOLOR}"

-include ${DEPS_BONUS}

.PHONY:		bonus

##########################################
####	CUSTOM TARGET					##

${OBJS}:			${BUILD}%.o:	%.c | ${MAND_D}
					@${CC} ${CFLAGS} -c ${CPPFLAGS} $< -o $@
					@echo "${CCOMPILE}${INDENT} Compiling $@${NOCOLOR}"

${OBJS_BONUS}:		${BUILD}%.o:	%.c | ${BONUS_D}
					@${CC} ${CFLAGS} -c ${CPPFLAGS} $< -o $@
					@echo "${CCOMPILE}${INDENT} Compiling $@${NOCOLOR}"

${MAND_D}:			|	${BUILD}${SRC}
					@mkdir -p ${addprefix ${BUILD}${SRC}, ${MAND_D}}

${BONUS_D}:			|	${BUILD}${SRC}
					@mkdir -p ${addprefix ${BUILD}${SRC}, ${BONUS_D}}

${BUILD}${SRC}:
					@mkdir -p ${BUILD}
					@mkdir -p ${addprefix ${BUILD}, ${SRC}}

##########################################
####	LIBRARIES TARGET				##

libraries:	${LIBFT}

${LIBFT}:
			${MAKE} ${dir ${LIBFT}} ft_printf file

.PHONY:	libraries ${LIBFT}

##########################################
####	CTAGS							##

CTAGS_FILE	:=	${SRCS}										\
				${shell ls ${dir ${LIBFT}}src/*/*.c}		\
				${shell ls ${INCLUDE}*.h}					\
				${shell ls ${dir ${LIBFT}}${INCLUDE}*.h}	\

tags:		${CTAGS_FILE}
			@ctags  ${CTAGS_FILE}
			@echo "${CCTAGS}${INDENT} Making $@${NOCOLOR}"

ctags:
			@${RM} tags
			@echo "${CCLEAN}${INDENT} Deleting ctags${NOCOLOR}"

.PHONY:		ctags
