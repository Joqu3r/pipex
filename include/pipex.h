/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 20:59:53 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 00:26:20 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include "libft_ft_printf.h"
# include "libft_file.h"
# include <sys/types.h>
# include <sys/wait.h>
# include <string.h>
# include <errno.h>

typedef struct s_pipex_pipe
{
	int	odd[2];
	int	even[2];
}	t_pipex_pipe;

typedef struct s_pipex_childs
{
	int		size;
	int		*wstatus;
	int		*pid;
}	t_pipex_c;

typedef struct s_pipex_param
{
	int				ret;
	int				state;
	char			*name;
	char			**cmd;
	char			**paths;
	t_pipex_pipe	pipe;
}	t_pipex_p;

typedef struct s_pipex_args
{
	char	*in;
	char	*out;
	int		nb_cmds;
	char	**cmds;
	char	**envp;
}	t_pipex_a;

/*	DEFINE ERRORS	*/

# define E_ARGC 500
# define E_ALLOC 501
# define E_FILE 502
# define E_FISDIR 503
# define E_FORK 504
# define E_CLOSE 505
# define E_PIPE 506
# define E_DUP 507
# define E_CMDNF 508

/*	INIT	*/

t_pipex_a	init_args(int argc, char **argv, char **envp);
t_pipex_p	*init_param(char **envp, char *name);
t_pipex_c	*init_childs(t_pipex_p *param, int size);
void		set_paths(t_pipex_p *param, char **envp);

/*	FILES COMPUTE	*/

void		compute_in_file(t_pipex_a args, t_pipex_p *param);
int			compute_out_file(t_pipex_a args, t_pipex_p *param);

/*	EXEC CMDS		*/

void		exe_cmds(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs);
void		exe_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs);
void		exe_last_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs);
void		exe_first_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs);

/*	HANGLING EXIT	*/

void		exit_program(int errc, char *err, t_pipex_p *param, \
t_pipex_c *childs);

/*	HANDLING ERRORS	*/

int			cmd_exist(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs);
void		error_handler(int errcode, char *error, char *name);
void		close_pipe(t_pipex_pipe pipe, int state);

#endif
