/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handler_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/19 15:56:27 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/13 22:23:02 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

static void	print_usage(void)
{
	ft_putstr_fd("Usage: ", 2);
	ft_putstr_fd("./pipex ", 2);
	ft_putstr_fd("<in file> ", 2);
	ft_putstr_fd("<cmd> ", 2);
	ft_putstr_fd("... ", 2);
	ft_putstr_fd("<cmdn> ", 2);
	ft_putendl_fd("<out file>", 2);
	ft_putstr_fd("Usage: ", 2);
	ft_putstr_fd("./pipex ", 2);
	ft_putstr_fd("here_doc ", 2);
	ft_putstr_fd("LIMITER ", 2);
	ft_putstr_fd("<cmd1> ", 2);
	ft_putstr_fd("... ", 2);
	ft_putstr_fd("<cmdn> ", 2);
	ft_putendl_fd("<out file>", 2);
}

static int	test_cmd(char *cmd, char *path)
{
	char	*tmp;

	tmp = ft_strjoin(path, cmd);
	if (!access(tmp, F_OK | X_OK))
	{
		free(tmp);
		return (1);
	}
	free(tmp);
	return (0);
}

int	cmd_exist(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	int		i;

	param->cmd = ft_split(args.cmds[param->state], ' ');
	if (!param->cmd)
		exit_program(E_ALLOC, NULL, param, childs);
	if (param->cmd[0] && !access(param->cmd[0], F_OK | X_OK))
	{
		ft_free_strs(param->cmd);
		return (1);
	}
	i = 0;
	while (param->cmd[0] && param->paths && param->paths[i])
	{
		if (test_cmd(param->cmd[0], param->paths[i++]))
		{
			ft_free_strs(param->cmd);
			return (1);
		}
	}
	if (param->cmd[0])
		error_handler(E_CMDNF, param->cmd[0], param->name);
	else
		error_handler(E_CMDNF, "''", param->name);
	ft_free_strs(param->cmd);
	return (0);
}

void	error_handler(int errcode, char *error, char *name)
{
	char	*strerr;
	char	*tmp;

	if (errcode == 0 || !ft_strncmp(error, "END", 4))
		return ;
	if (errcode == E_ARGC)
		return (print_usage());
	strerr = ft_strjoin(name, ": ");
	if (error)
	{
		tmp = ft_strjoin(strerr, error);
		free(strerr);
		strerr = ft_strjoin(tmp, ": ");
		free(tmp);
	}
	if (errcode != E_CMDNF && errno > 0)
		tmp = ft_strjoin(strerr, strerror(errno));
	else
		tmp = ft_strjoin(strerr, "command not found");
	free(strerr);
	strerr = tmp;
	ft_putendl_fd(strerr, 2);
	free(strerr);
}
