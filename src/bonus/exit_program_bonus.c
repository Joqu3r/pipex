/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_program_bonus.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/19 16:23:32 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 02:40:12 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

static void	close_common_fds(t_pipex_p *param, t_pipex_c *childs)
{
	if (param && param->state < childs->size \
		&& !childs->pid[param->state])
	{
		close(0);
		close(1);
		close(2);
	}
}

static void	set_errno(int *errc, t_pipex_p *param, t_pipex_c *childs)
{
	if (*errc == E_ARGC)
		errno = EINVAL;
	else if (*errc == E_CMDNF)
		errno = EKEYEXPIRED;
	if (!param || (param && param->state < childs->size - 1)
		|| *errc == E_CMDNF)
		*errc = errno;
}

static void	free_struct(t_pipex_p *param, t_pipex_c *childs)
{
	if (param && param->paths)
		ft_free_strs(param->paths);
	if (param && param->cmd
		&& (param->state < childs->size && childs->pid[param->state] == 0))
		ft_free_strs(param->cmd);
	if (childs && childs->pid)
		free(childs->pid);
	if (param && childs->wstatus)
		free(childs->wstatus);
}

void	exit_program(int errc, char *err, t_pipex_p *param, t_pipex_c *childs)
{
	if (param)
		error_handler(errc, err, param->name);
	else
		error_handler(errc, err, "./pipex");
	if (param && childs)
		close_common_fds(param, childs);
	if (param && param->state < childs->size - 1)
		close_pipe(param->pipe, param->state);
	free_struct(param, childs);
	set_errno(&errc, param, childs);
	if (param)
		free(param);
	if (childs)
		free(childs);
	exit(errc);
}
