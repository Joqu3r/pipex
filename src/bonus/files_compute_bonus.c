/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   files_compute_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/21 21:24:00 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 02:41:09 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

static int	open_out_file(t_pipex_a args, t_pipex_p *param)
{
	if (param->heredoc)
		return (open(args.out, O_CREAT | O_WRONLY | O_APPEND, \
	S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
	else
		return (open(args.out, O_CREAT | O_WRONLY | O_TRUNC, \
	S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
}

static int	create_out_file(t_pipex_a args, t_pipex_p *param)
{
	int		*fd;

	if (param->state % 2 == 0)
	{	
		fd = &(param->pipe.even[1]);
		param->pipe.odd[1] = -1;
	}
	else
	{	
		fd = &(param->pipe.odd[1]);
		param->pipe.even[1] = -1;
	}
	*fd = open_out_file(args, param);
	if (*fd == -1)
	{
		error_handler(E_FILE, args.out, param->name);
		return (-1);
	}
	return (0);
}

int	compute_out_file(t_pipex_a args, t_pipex_p *param)
{
	int	fd;

	fd = open(args.out, O_DIRECTORY);
	if (fd != -1)
	{
		close(fd);
		errno = EISDIR;
		error_handler(E_FISDIR, args.out, param->name);
		return (-1);
	}
	if (!access(args.out, F_OK)
		&& access(args.out, W_OK) == -1)
	{
		error_handler(E_FILE, args.out, param->name);
		return (-1);
	}
	return (create_out_file(args, param));
}

static void	read_stdin(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	char	*tmp_limiter;
	char	*line;

	tmp_limiter = ft_strjoin(args.limiter, "\n");
	if (!tmp_limiter)
		exit_program(E_ALLOC, NULL, param, childs);
	line = get_next_line(0);
	while (line && ft_strncmp(line, tmp_limiter, ft_strlen(tmp_limiter)))
	{
		write(param->pipe.odd[1], line, ft_strlen(line));
		free(line);
		line = get_next_line(0);
	}
	free(line);
	free(tmp_limiter);
	get_next_line(-42);
}

void	compute_in_file(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	if (param->heredoc)
	{
		if (pipe(param->pipe.odd) == -1)
			exit_program(E_PIPE, "pipe", param, childs);
		read_stdin(args, param, childs);
		close(param->pipe.odd[1]);
		param->pipe.odd[1] = -1;
	}
	else
	{
		if (access(args.in, F_OK | R_OK) == -1)
			param->pipe.odd[0] = -1;
		else
			param->pipe.odd[0] = open(args.in, O_RDONLY);
		param->pipe.odd[1] = -1;
	}
}
