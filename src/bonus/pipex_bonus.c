/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 21:00:31 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/09 15:16:51 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

static void	wait_for_all_pids(t_pipex_p *param, t_pipex_c *childs)
{
	int	i;

	i = 0;
	while (i < childs->size)
	{
		if (childs->pid[i] != -1)
		{
			waitpid(childs->pid[i], &(childs->wstatus[i]), 0);
			if (WIFEXITED(childs->wstatus[i]) && param->ret != 1
				&& param->ret != E_CMDNF)
				param->ret = WEXITSTATUS(childs->wstatus[i]);
			else if (param->ret != 1 && param->ret != E_CMDNF)
				param->ret = 0;
		}
		i++;
	}
	return ;
}

int	main(int argc, char **argv, char **envp)
{
	t_pipex_a	args;
	t_pipex_p	*param;
	t_pipex_c	*childs;

	if (argc < 5)
		exit_program(E_ARGC, NULL, NULL, NULL);
	param = init_param(argc, argv, envp);
	args = init_args(argc, argv, envp, param->heredoc);
	childs = init_childs(param, args.nb_cmds);
	exe_cmds(args, param, childs);
	wait_for_all_pids(param, childs);
	exit_program(param->ret, "END", param, childs);
}
