/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/19 16:59:22 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/09 17:57:58 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex_bonus.h"

t_pipex_c	*init_childs(t_pipex_p *param, int size)
{
	t_pipex_c	*childs;
	int			i;

	childs = ft_calloc(1, sizeof(*childs));
	if (!childs)
		exit_program(E_ALLOC, NULL, param, childs);
	childs->size = size;
	childs->wstatus = ft_calloc(childs->size, sizeof(*(childs->wstatus)));
	childs->pid = ft_calloc(childs->size, sizeof(*(childs->pid)));
	if (!childs->wstatus || !childs->pid)
		exit_program(E_ALLOC, NULL, param, childs);
	i = 0;
	while (i < size)
	{
		childs->pid[i] = -1;
		i++;
	}
	return (childs);
}

static void	set_default_value_pipe(t_pipex_pipe *pipe)
{
	pipe->odd[0] = -1;
	pipe->odd[1] = -1;
	pipe->even[0] = -1;
	pipe->even[1] = -1;
}

t_pipex_p	*init_param(int argc, char **argv, char **envp)
{
	t_pipex_p	*param;

	if (!ft_strncmp(argv[1], "here_doc", 9) && argc < 6)
		exit_program(E_ARGC, NULL, NULL, NULL);
	param = ft_calloc(1, sizeof(*param));
	if (!param)
		exit_program(E_ALLOC, NULL, param, NULL);
	param->heredoc = (!ft_strncmp(argv[1], "here_doc", 9));
	param->ret = 0;
	param->state = 0;
	param->name = argv[0];
	set_paths(param, envp);
	set_default_value_pipe(&(param->pipe));
	return (param);
}

t_pipex_a	init_args(int argc, char **argv, char **envp, int heredoc)
{
	t_pipex_a	args;

	if (heredoc)
		args.in = NULL;
	else
		args.in = argv[1];
	if (heredoc)
		args.limiter = argv[2];
	else
		args.limiter = NULL;
	args.out = argv[argc - 1];
	args.nb_cmds = argc - (3 + heredoc);
	args.cmds = &(argv[2 + heredoc]);
	args.envp = envp;
	return (args);
}
