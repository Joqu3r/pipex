/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute_cmds.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/10 21:00:31 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/13 22:24:15 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

void	close_pipe(t_pipex_pipe pipe, int state)
{
	if (state % 2 == 0)
	{
		if (pipe.odd[0] != -1)
			close(pipe.odd[0]);
		if (pipe.even[1] != -1)
			close(pipe.even[1]);
	}
	else
	{
		if (pipe.odd[1] != -1)
			close(pipe.odd[1]);
		if (pipe.even[0] != -1)
			close(pipe.even[0]);
	}
}

void	exe_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	char	*tmp;
	int		ret;
	int		i;

	param->cmd = ft_split(args.cmds[param->state], ' ');
	if (!param->cmd)
		exit_program(E_ALLOC, NULL, param, childs);
	ret = execve(param->cmd[0], param->cmd, args.envp);
	i = 0;
	while (ret == -1 && param->paths && param->paths[i])
	{
		tmp = ft_strjoin(param->paths[i], param->cmd[0]);
		ret = execve(tmp, param->cmd, args.envp);
		free(tmp);
		i++;
	}
	if (ret == -1)
		exit_program(E_CMDNF, param->cmd[0], param, childs);
}

void	exe_cmds(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	exe_first_cmd(args, param, childs);
	close_pipe(param->pipe, param->state);
	param->state++;
	exe_last_cmd(args, param, childs);
	close_pipe(param->pipe, param->state);
}
