/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute_first_cmd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/26 03:00:00 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 00:21:38 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

void	exe_first_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	if (pipe(param->pipe.even) == -1)
		exit_program(E_PIPE, "pipe", param, childs);
	compute_in_file(args, param);
	if (param->pipe.odd[0] == -1)
		return (error_handler(E_FILE, args.in, param->name));
	if (cmd_exist(args, param, childs))
		childs->pid[param->state] = fork();
	else
	{
		childs->pid[param->state] = -1;
		return ;
	}
	if (childs->pid[param->state] == -1)
		exit_program(E_FORK, NULL, param, childs);
	else if (childs->pid[param->state] != 0)
		return ;
	close(param->pipe.even[0]);
	if (dup2(param->pipe.odd[0], 0) == -1)
		exit_program(E_DUP, "dup", param, childs);
	if (dup2(param->pipe.even[1], 1) == -1)
		exit_program(E_DUP, "dup", param, childs);
	close_pipe(param->pipe, param->state);
	exe_cmd(args, param, childs);
}
