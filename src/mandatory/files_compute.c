/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   files_compute.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/21 21:24:00 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 00:25:04 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static int	create_out_file(t_pipex_a args, t_pipex_p *param)
{
	int		*fd;

	if (param->state % 2 == 0)
	{	
		fd = &(param->pipe.even[1]);
		param->pipe.odd[1] = -1;
	}
	else
	{	
		fd = &(param->pipe.odd[1]);
		param->pipe.even[1] = -1;
	}
	*fd = open(args.out, O_CREAT | O_WRONLY | O_TRUNC, \
	S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (*fd == -1)
	{
		error_handler(E_FILE, args.out, param->name);
		return (-1);
	}
	return (0);
}

int	compute_out_file(t_pipex_a args, t_pipex_p *param)
{
	int	fd;

	fd = open(args.out, O_DIRECTORY);
	if (fd != -1)
	{
		close(fd);
		errno = EISDIR;
		error_handler(E_FISDIR, args.out, param->name);
		return (-1);
	}
	if (!access(args.out, F_OK) && access(args.out, W_OK) == -1)
	{
		error_handler(E_FILE, args.out, param->name);
		return (-1);
	}
	return (create_out_file(args, param));
}

void	compute_in_file(t_pipex_a args, t_pipex_p *param)
{
	if (access(args.in, F_OK | R_OK) == -1)
		param->pipe.odd[0] = -1;
	else
		param->pipe.odd[0] = open(args.in, O_RDONLY);
	param->pipe.odd[1] = -1;
}
