/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/01 21:37:44 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 18:46:36 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static void	add_last_slash(t_pipex_p *param)
{
	int		i;
	char	*tmp;

	i = 0;
	while (param->paths[i])
	{
		tmp = ft_strjoin(param->paths[i], "/");
		if (!tmp)
			exit_program(E_ALLOC, NULL, param, NULL);
		free(param->paths[i]);
		param->paths[i] = tmp;
		i++;
	}
}

void	set_paths(t_pipex_p *param, char **envp)
{
	int		i;
	char	*tmp;

	i = 0;
	while (envp[i] && ft_strncmp(envp[i], "PATH=", 5))
		i++;
	if (!envp[i])
	{
		param->paths = NULL;
		return ;
	}
	param->paths = ft_split(envp[i], ':');
	if (!param->paths)
		exit_program(E_ALLOC, NULL, param, NULL);
	tmp = ft_strdup(param->paths[0] + 5);
	if (!tmp)
		exit_program(E_ALLOC, NULL, param, NULL);
	free(param->paths[0]);
	param->paths[0] = tmp;
	add_last_slash(param);
}
