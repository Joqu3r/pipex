/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute_last_cmd.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angoulem      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/24 23:10:00 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 00:24:44 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static void	exe_last_odd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	if (dup2(param->pipe.even[0], 0) == -1)
		exit_program(E_DUP, "dup", param, childs);
	if (dup2(param->pipe.odd[1], 1) == -1)
		exit_program(E_DUP, "dup", param, childs);
	close_pipe(param->pipe, param->state);
	exe_cmd(args, param, childs);
}

static void	exe_last_even(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	if (dup2(param->pipe.odd[0], 0) == -1)
		exit_program(E_DUP, "dup", param, childs);
	if (dup2(param->pipe.even[1], 1) == -1)
		exit_program(E_DUP, "dup", param, childs);
	close_pipe(param->pipe, param->state);
	exe_cmd(args, param, childs);
}

void	exe_last_cmd(t_pipex_a args, t_pipex_p *param, t_pipex_c *childs)
{
	if (compute_out_file(args, param) == -1)
	{
		param->ret = 1;
		return ;
	}
	if (cmd_exist(args, param, childs))
		childs->pid[param->state] = fork();
	else
	{
		childs->pid[param->state] = -1;
		param->ret = E_CMDNF;
		return ;
	}
	if (childs->pid[param->state] == -1)
		exit_program(E_FORK, NULL, param, childs);
	else if (childs->pid[param->state] != 0)
		return ;
	if (param->state % 2 == 0)
		exe_last_even(args, param, childs);
	else
		exe_last_odd(args, param, childs);
}
