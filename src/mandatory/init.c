/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thugueno <thugueno@student.42angouleme.fr  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/19 16:59:22 by thugueno          #+#    #+#             */
/*   Updated: 2023/05/10 18:46:01 by thugueno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

t_pipex_c	*init_childs(t_pipex_p *param, int size)
{
	t_pipex_c	*childs;
	int			i;

	childs = ft_calloc(1, sizeof(*childs));
	if (!childs)
		exit_program(E_ALLOC, NULL, param, childs);
	childs->size = size;
	childs->wstatus = ft_calloc(childs->size, sizeof(*(childs->wstatus)));
	childs->pid = ft_calloc(childs->size, sizeof(*(childs->pid)));
	if (!childs->wstatus || !childs->pid)
		exit_program(E_ALLOC, NULL, param, childs);
	i = 0;
	while (i < size)
	{
		childs->pid[i] = -1;
		i++;
	}
	return (childs);
}

static void	set_default_value_pipe(t_pipex_pipe *pipe)
{
	pipe->odd[0] = -1;
	pipe->odd[1] = -1;
	pipe->even[0] = -1;
	pipe->even[1] = -1;
}

t_pipex_p	*init_param(char **envp, char *name)
{
	t_pipex_p	*param;

	param = ft_calloc(1, sizeof(*param));
	if (!param)
		exit_program(E_ALLOC, NULL, param, NULL);
	param->ret = 0;
	param->state = 0;
	param->name = name;
	set_paths(param, envp);
	set_default_value_pipe(&(param->pipe));
	return (param);
}

t_pipex_a	init_args(int argc, char **argv, char **envp)
{
	t_pipex_a	args;

	args.in = argv[1];
	args.out = argv[argc - 1];
	args.nb_cmds = argc - 3;
	args.cmds = &(argv[2]);
	args.envp = envp;
	return (args);
}
